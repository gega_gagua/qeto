import { Suspense, lazy } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import { Box } from "@mui/material";
import { theme } from "./theme";

const MainLayout = lazy(() => import("./layout/MainLayout"));

const pageLoading = "";

function App() {
  return (
    <Suspense fallback={pageLoading}>
      <ThemeProvider theme={theme}>
        <Box sx={{ display: "flex" }}>
          <BrowserRouter>
            <Routes>
              <Route
                path="/"
                element={<Navigate replace to="/inner/profile" />}
              />
              <Route path="/inner/*" element={<MainLayout />} />
            </Routes>
          </BrowserRouter>
        </Box>
      </ThemeProvider>
    </Suspense>
  );
}

export default App;
