import {
  Box,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Typography,
  Button
} from "@mui/material";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";
import image from "assets/images/logout.png";

const Popup = ({ open, close }) => {
  return (
    <Dialog
      open={open}
      keepMounted
      aria-describedby="alert-dialog-slide-description"
      PaperProps={{
        style: {
          borderRadius: "20px",
          maxWidth: "460px",
          padding: "32px 18px 32px 32px"
        }
      }}
    >
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "end"
        }}
      >
        <CancelOutlinedIcon
          onClick={close}
          sx={{ cursor: "pointer", color: "rgba(84, 94, 108, 1)" }}
        />
      </Box>

      <Box
        component="img"
        sx={{
          width: 90
        }}
        alt="log out"
        src={image}
      />
      <DialogTitle sx={{ padding: "20px 0" }}>
        <Typography variant="h4" component="div">
          Are you sure you want to sign out?
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ padding: "20px 0" }}>
        <DialogContentText
          id="alert-dialog-slide-description"
          sx={{
            color: "#545E6C",
            fontSize: "20px",
            lineHeight: "30px"
          }}
        >
          You won’t be able to use your dashboard or collect data through your
          Datacy extension until you sign back in.
        </DialogContentText>
      </DialogContent>

      <DialogActions sx={{ padding: 0 }}>
        <Button variant="outlined" onClick={close} fullWidth>
          Cancel
        </Button>
        <Button variant="contained" onClick={close} fullWidth>
          Log Out
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default Popup;
