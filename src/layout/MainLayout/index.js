import { useState } from "react";
import { Box } from "@mui/material";
import Navigation from "./Navigation";
import Header from "./Header";
import Routing from "./Routing";
import Footer from "./Footer";
import Popup from "./Popup";
import Notifications from "component/Notifications";

const MainLayout = () => {
  const [open, setOpen] = useState(false);
  const [notifications, setNotifications] = useState(false);

  return (
    <>
      <Navigation />
      <Box
        sx={{
          width: 1,
          display: "flex",
          flexDirection: "column"
        }}
      >
        <Header
          logout={() => setOpen(true)}
          OpenNotifications={() => setNotifications(true)}
        />
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: 1
          }}
        >
          <Routing />
        </Box>
        <Footer />
      </Box>

      <Notifications
        notifications={notifications}
        closeNotifications={() => setNotifications(false)}
      />

      <Popup open={open} close={() => setOpen(false)} />
    </>
  );
};

export default MainLayout;
