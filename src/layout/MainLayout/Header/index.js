import SearchIcon from "@mui/icons-material/Search";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import styles from "./styles.module.scss";

const Header = ({ logout, OpenNotifications }) => {
  return (
    <header>
      <h2>Profile</h2>
      <div className={styles.icons}>
        <SearchIcon />
        <NotificationsNoneIcon onClick={OpenNotifications} />
        <PersonOutlineIcon onClick={logout} />
      </div>
    </header>
  );
};

export default Header;
