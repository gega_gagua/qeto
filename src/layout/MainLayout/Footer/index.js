import { Link } from "react-router-dom";
import { Button } from "@mui/material";
import styles from "./styles.module.scss";

const Footer = () => {
  return (
    <div className={styles.footer}>
      <div className={styles.content}>
        <Link to="/" className={styles["logo-wrapper"]}>
          Cancel
        </Link>
        <Button variant="contained">Save & Close</Button>
      </div>
    </div>
  );
};

export default Footer;
