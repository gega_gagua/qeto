import { Link } from "react-router-dom";
import { Chip } from "@mui/material";
import NavItem from "./NavItem";
import styles from "./styles.module.scss";

const nav = [
  {
    label: "Home",
    to: "/",
    exact: "true"
  },
  {
    label: "Data",
    to: "/"
  },
  {
    label: "Earnings",
    to: "/"
  },
  {
    label: "Apps",
    to: "/"
  },
  {
    label: "Restrictions",
    to: "/"
  }
];

const Navigation = () => {
  return (
    <nav className={styles.nav}>
      <Link to="/" className={styles["logo-wrapper"]}>
        datacy
      </Link>

      <div className={styles["nav-items"]}>
        {nav.map((item, i) => (
          <NavItem key={i} to={item.to} exact={item.exact}>
            {item.label}
          </NavItem>
        ))}
      </div>

      <div>
        <div className={styles["nav-footer"]}>
          <Link to="/">Invite Friends</Link>
          <Chip label="$10" className={styles.price} />
        </div>

        <NavItem to="/" className={styles.terms}>
          Terms and Privacy
        </NavItem>
      </div>
    </nav>
  );
};

export default Navigation;
