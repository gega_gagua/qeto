import { NavLink } from "react-router-dom";
import styles from "./styles.module.scss";

const NavItem = ({ to, children, exact, className }) => {
  return (
    <NavLink
      to={to}
      className={`${styles["nav-item"]} ${className ? className : ""}`}
      exact={exact}
    >
      <span>{children}</span>
    </NavLink>
  );
};

export default NavItem;
