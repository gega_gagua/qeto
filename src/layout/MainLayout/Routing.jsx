import { Routes, Route, Navigate } from "react-router-dom";
import InviteFriends from "views/InviteFriends";
import Profile from "views/Profile";

const Routing = () => {
  return (
    <Routes>
      <Route path="" element={<Navigate replace to="profile" />} />
      <Route path="profile" element={<Profile />} />
      <Route path="invite-friends" element={<InviteFriends />} />
    </Routes>
  );
};

export default Routing;
