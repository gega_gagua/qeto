import { Box, TextField, Typography, Chip } from "@mui/material";
import Link from "@mui/material/Link";
import linkImg from "assets/images/link.png";
import facebook from "assets/images/facebook.png";
import telegram from "assets/images/telegram.png";

const InviteLinks = () => {
  return (
    <Box sx={{ marginRight: "90px" }}>
      <Link href="#" underline="none" color="inherit">
        Referral link
      </Link>
      <Box
        sx={{
          backgroundColor: "#fff",
          borderRadius: "20px",
          padding: "32px 52px",
          marginTop: "15px",
          boxShadow: "0px 2px 6px rgba(0, 0, 0, 0.1)"
        }}
      >
        <TextField
          variant="outlined"
          id="outlined-basic"
          value="datacy.com/join/4781717"
          sx={{ width: 1 }}
          inputProps={{ style: { textAlign: "center" } }}
        />

        <Box sx={{ display: "flex", alignItems: "center", marginTop: "32px" }}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "50%",
                backgroundColor: "#47107E",
                height: "52px",
                width: "52px",
                marginBottom: "8px"
              }}
            >
              <Box component="img" alt="Copy link" src={linkImg} />
            </Box>
            <Chip
              label="Copy link"
              color="primary"
              variant="outlined"
              clickable
            />
          </Box>

          <Typography
            variant="body1"
            sx={{ fontSize: 20, lineHeight: 1.5, margin: "0 50px" }}
          >
            or
          </Typography>

          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginRight: "30px"
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "50%",
                backgroundColor: "#1778F2",
                height: "52px",
                width: "52px",
                marginBottom: "8px"
              }}
            >
              <Box component="img" alt="Facebook" src={facebook} />
            </Box>

            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginRight: "30px"
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "50%",
                backgroundColor: "#00ACEE",
                height: "52px",
                width: "52px",
                marginBottom: "8px"
              }}
            >
              <Box component="img" alt="Twitter" src={linkImg} />
            </Box>
            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              marginRight: "30px"
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "50%",
                backgroundColor: "#FF395A",
                height: "52px",
                width: "52px",
                marginBottom: "8px"
              }}
            >
              <Box component="img" alt="Linkedin" src={linkImg} />
            </Box>
            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>

          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center"
            }}
          >
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                borderRadius: "50%",
                backgroundColor: "#0088CC",
                height: "52px",
                width: "52px",
                marginBottom: "8px"
              }}
            >
              <Box component="img" alt="Telegram" src={telegram} />
            </Box>
            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default InviteLinks;
