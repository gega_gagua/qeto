import { Box } from "@mui/material";
import InviteLinks from "./InviteLinks";
import InviteText from "./InviteText";

const InviteFriends = () => {
  return (
    <Box sx={{ display: "flex", alignItems: "end", padding: "30px" }}>
      <InviteLinks />
      <InviteText />
    </Box>
  );
};

export default InviteFriends;
