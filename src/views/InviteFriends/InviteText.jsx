import { Box, Typography } from "@mui/material";
import image from "assets/images/invite.png";

const InviteText = () => {
  return (
    <Box sx={{ maxWidth: "480px" }}>
      <Box
        component="img"
        sx={{
          width: 72
        }}
        alt="invite friends"
        src={image}
      />
      <Typography variant="h4" sx={{ margin: "20px 0" }}>
        Invite a fariend. Earn $10
      </Typography>

      <Typography variant="body1" sx={{ fontSize: 20, lineHeight: 1.5 }}>
        We can’t transform the data industry alone, so share your link with
        someone who would benefit from using Datacy.You’ll both receive an extra
        $10 when they make their first withdrawal.
      </Typography>
    </Box>
  );
};

export default InviteText;
