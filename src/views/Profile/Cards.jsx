import {
  Box,
  Card,
  Chip,
  // Slider,
  TextField,
  Select,
  FormGroup,
  FormControlLabel,
  Checkbox,
  MenuItem,
  Typography
} from "@mui/material";
import { styled } from "@mui/material/styles";
import MuiSlider from "@mui/material/Slider";

const CardContainer = styled(Card)(() => ({
  display: "flex",
  alignItems: "center",
  marginBottom: "0.75rem",
  padding: "1.5rem",
  borderRadius: "0.75rem",
  color: "#232737",
  boxShadow: "0px 2px 6px rgba(0, 0, 0, 0.1)"
}));

const Question = styled(Box)(() => ({
  width: "40%",
  paddingRight: "2.5rem"
}));

export const StyledTextField = styled(TextField)({
  width: "100%",
  marginTop: "0.75rem",

  "& fieldset": {
    borderRadius: "8px"
  },

  "& input::placeholder": {
    fontWeight: 300
  },

  "& input": {
    fontWeight: 200,
    height: "1rem"
  }
});

export const StyledSelect = styled(Select)(({ theme }) => ({
  width: "100%",

  "& fieldset": {
    borderRadius: "8px"
  },

  "& input::placeholder": {
    fontWeight: 300
  },

  "& input": {
    fontFamily: "IBM Plex Sans",
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(1.5),
    height: "1rem",
    fontWeight: 100
  },

  "& .MuiSelect-select": {
    fontFamily: "IBM Plex Sans",
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(1.5),
    fontWeight: 100
  }
}));

const StyledChip = ({ active, ...props }) => {
  let style = {
    color: "#47107E",
    backgroundColor: "#F3F3F5",
    lineHeight: "1.5rem",
    marginRight: "0.75rem",
    marginBottom: "0.75rem"
  };

  if (active) {
    style = {
      backgroundColor: "#47107E",
      color: "#fff",
      lineHeight: "1.5rem",
      marginRight: "0.75rem",
      marginBottom: "0.75rem"
    };
  }

  return <Chip sx={style} {...props} />;
};

export const Slider = styled(MuiSlider)(({ theme }) => ({
  "& .MuiSlider-rail": {
    opacity: 1,
    color: "#F3F3F5",
    height: "0.75rem"
  },
  "& .MuiSlider-track": {
    color: "#47107E",
    height: "0.75rem"
  },
  "& .MuiSlider-mark": {
    color: "#47107E"
  },
  "& .MuiSlider-markLabel": {
    paddingTop: "0",
    fontFamily: "Pangram",
    fontSize: "0.75rem",
    fontStyle: "normal",
    fontWeight: "300",
    lineHeight: 1.8,
    letterSpacing: 0,
    color: "#545E6C"
  },
  "& .MuiSlider-thumb": {
    height: "2rem",
    width: "2rem"
  }
}));

const gender = [
  { label: "Male", active: false },
  { label: "Female", active: true },
  { label: "Non-binary", active: false },
  { label: "Other", active: false }
];

const marital = [
  { label: "Single", active: false },
  { label: "Married", active: false },
  { label: "Divorced/Separted", active: false },
  { label: "Other", active: true }
];

const work = [
  { label: "I work full-time", active: false },
  { label: "I work part-time ", active: false },
  { label: "I’m retired", active: false },
  { label: "I’m not working", active: true },
  { label: "I’m in school", active: false },
  { label: "Other", active: false }
];

const Cards = () => {
  return (
    <>
      <CardContainer>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            How old are you?
          </Typography>
        </Question>

        <Box width="60%">
          <Slider
            aria-label="Temperature"
            defaultValue={30}
            valueLabelDisplay="auto"
            variant="primary"
          />
        </Box>
      </CardContainer>

      <CardContainer>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            How would you describe your gender?
          </Typography>
        </Question>

        <Box width="60%">
          {gender.map((item) => (
            <StyledChip
              key={item.label}
              label={item.label}
              active={item.active}
            />
          ))}
        </Box>
      </CardContainer>

      <CardContainer>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            Which group(s) do you most identify with?
          </Typography>
          <Typography variant="body2" color="#232737">
            Choose as many as you want
          </Typography>
        </Question>

        <Box width="60%">
          <FormGroup sx={{ flexDirection: "row" }}>
            <FormControlLabel
              control={<Checkbox defaultChecked />}
              label="White"
            />
            <FormControlLabel control={<Checkbox />} label="South Asian" />
            <FormControlLabel control={<Checkbox />} label="Black" />
            <FormControlLabel control={<Checkbox />} label="Latinx" />
            <FormControlLabel control={<Checkbox />} label="Indegenous" />
            <FormControlLabel control={<Checkbox />} label="East Asian" />
            <FormControlLabel control={<Checkbox />} label="Other" />
          </FormGroup>
        </Box>
      </CardContainer>

      <CardContainer>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            What’s your marital status?
          </Typography>
        </Question>

        <Box width="60%">
          {marital.map((item) => (
            <StyledChip
              key={item.label}
              label={item.label}
              active={item.active}
            />
          ))}

          <StyledTextField
            id="outlined-basic"
            label="Describe your marital status"
            variant="outlined"
          />
        </Box>
      </CardContainer>

      <CardContainer>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            What’s the highest degree you’ve earned?
          </Typography>
        </Question>

        <Box width="60%">
          <StyledSelect
            value={""}
            displayEmpty
            inputProps={{ "aria-label": "Without label" }}
          >
            <MenuItem value="">
              <em>Associate</em>
            </MenuItem>
            <MenuItem value={1}>Associate</MenuItem>
            <MenuItem value={2}>Associate</MenuItem>
            <MenuItem value={3}>Associate</MenuItem>
          </StyledSelect>
        </Box>
      </CardContainer>

      <CardContainer>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            What’s your work status?
          </Typography>
          <Typography variant="body2" color="#232737">
            Choose as many as you want
          </Typography>
        </Question>

        <Box width="60%">
          {work.map((item) => (
            <StyledChip
              key={item.label}
              label={item.label}
              active={item.active}
            />
          ))}
        </Box>
      </CardContainer>

      <CardContainer sx={{ marginBottom: "5rem" }}>
        <Question>
          <Typography varinat="h3" fontSize={20} color="#232737">
            What’s your annual income?
          </Typography>
        </Question>

        <Box width="60%">
          <StyledSelect
            value={""}
            displayEmpty
            inputProps={{ "aria-label": "Without label" }}
          >
            <MenuItem value="">
              <em>$30,000 - $45,000</em>
            </MenuItem>
            <MenuItem value={1}>$45,000 - $60,000</MenuItem>
            <MenuItem value={2}>$60,000 - $75,000</MenuItem>
            <MenuItem value={3}>$75,000 - $90,000</MenuItem>
          </StyledSelect>
        </Box>
      </CardContainer>
    </>
  );
};

export default Cards;
