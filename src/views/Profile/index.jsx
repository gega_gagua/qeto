import { Box, Chip } from "@mui/material";
import { styled } from "@mui/material/styles";
import Welcome from "./Welcome";
import Cards from "./Cards";

const Container = styled(Box)(() => ({
  maxWidth: "51rem",
  margin: "1rem auto",
  padding: "0 1.5rem"
}));

const Profile = () => {
  return (
    <Container>
      <Welcome />

      <Box display="flex" mt={6.5} mb={2.5}>
        <Chip
          label="Demographics"
          sx={{ color: "#fff", backgroundColor: "#47107E" }}
        />
        <Chip
          label="Behaviour 3"
          sx={{
            marginLeft: "0.8rem",
            backgroundColor: "#fff",
            color: "#47107E"
          }}
        />
      </Box>

      <Cards />
    </Container>
  );
};

export default Profile;
