import { Box, Typography } from "@mui/material";
import image from "assets/images/welcome.png";

const Welcome = () => {
  return (
    <>
      <Box
        component="img"
        width={127}
        mb={3}
        alt="invite friends"
        src={image}
      />
      <Typography variant="h1" color="#232737" mb={2.5} fontSize={32}>
        Hey, Julia!
      </Typography>

      <Typography variant="h3" color="#545E6C" fontSize={20} lineHeight={1.5}>
        Welcome to your profile, where you can tell us a little more about you.
        We use this data to help match you with brands interested in licensings
        your data. To increase your earning potential, provide as much
        information as possible.
      </Typography>
    </>
  );
};

export default Welcome;
