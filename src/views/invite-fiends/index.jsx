import React from "react";
import { Box } from '@mui/material'
import InviteLinks from './InviteLinks'
import InviteText from './InviteText'
import { SideNavLayout } from '../../squirrel/components/layouts/SideNavLayout'

export const InviteFriends = () => {
  return (
    <SideNavLayout>
      <Box display="flex" alignItems="center" justifyContent="center" height={1}>
        <Box display="flex" alignItems='end' padding='30px'>
          <InviteLinks />
          <InviteText />
        </Box>
      </Box>
    </SideNavLayout>
  )
}
