import { Box, Typography } from "@mui/material";
import image from "../../img/invite.png";

const InviteText = () => {
  return (
    <Box sx={{ maxWidth: "480px" }}>
      <Box
        component="img"
        sx={{
          width: 72
        }}
        alt="invite friends"
        src={image}
      />
      <Typography variant="h4" margin="1.25rem 0">
        Invite a fariend. Earn $10
      </Typography>

      <Typography variant="body1" fontSize="1.25rem">
        We can’t transform the data industry alone, so share your link with
        someone who would benefit from using Datacy.You’ll both receive an extra
        $10 when they make their first withdrawal.
      </Typography>
    </Box>
  );
};

export default InviteText;
