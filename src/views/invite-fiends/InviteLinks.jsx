import React from 'react'
import { Box, TextField, Typography, Chip } from '@mui/material'
import Link from '@mui/material/Link'
import LinkImage from '../../img/link.png'
import FacebookImage from '../../img/facebook.png'
import TelegramImage from '../../img/telegram.png'
import { styled } from '@mui/material/styles'

const ImgContainer = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  borderRadius: '50%',
  height: '52px',
  width: '52px',
  marginBottom: '8px',
}))

const InviteLinks = () => {
  return (
    <Box marginRight="90px">
      <Link href="#" underline="none" color="inherit">
        Referral link
      </Link>
      <Box
        backgroundColor="#fff"
        borderRadius="20px"
        padding="32px 52px"
        marginTop="15px"
        boxShadow="0px 2px 6px rgba(0, 0, 0, 0.1)"
      >
        <TextField
          variant="outlined"
          id="outlined-basic"
          value="datacy.com/join/4781717"
          sx={{ width: 1 }}
          inputProps={{ style: { textAlign: 'center' } }}
        />

        <Box display="flex" alignItems="center" marginTop="32px">
          <Box display="flex" flexDirection="column" alignItems="center">
            <ImgContainer backgroundColor="#47107E">
              <img component="img" alt="Copy link" src={LinkImage} />
            </ImgContainer>
            <Chip
              label="Copy link"
              color="primary"
              variant="outlined"
              clickable
            />
          </Box>

          <Typography variant="body1" fontSize="1.25rem" margin="0 50px">
            or
          </Typography>

          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            marginRight="30px"
          >
            <ImgContainer backgroundColor="#1778F2">
              <img
                width={14}
                component="img"
                alt="Facebook"
                src={FacebookImage}
              />
            </ImgContainer>

            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            marginRight="30px"
          >
            <ImgContainer backgroundColor="#00ACEE">
              <img component="img" alt="Twitter" src={LinkImage} />
            </ImgContainer>
            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>

          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            marginRight="30px"
          >
            <ImgContainer backgroundColor="#FF395A">
              <img component="img" alt="Linkedin" src={LinkImage} />
            </ImgContainer>
            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>

          <Box display="flex" flexDirection="column" alignItems="center">
            <ImgContainer backgroundColor="#0088CC">
              <img component="img" alt="Telegram" src={TelegramImage} />
            </ImgContainer>
            <Chip label="Share" color="primary" variant="outlined" clickable />
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default InviteLinks
