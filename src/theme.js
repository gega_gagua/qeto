import { createTheme } from "@mui/material/styles";

export const theme = createTheme({
  typography: {
    body1: {
      color: "#545E6C"
    },
    h4: {
      color: "#232737"
    }
  },
  palette: {
    primary: {
      main: "#47107E"
    }
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: "0.75rem",
          padding: "14px 30px"
        }
      }
    }
  }
});
