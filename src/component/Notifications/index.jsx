import React, { useState } from "react";
import { Box, Chip, Typography } from "@mui/material";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import CloseOutlinedIcon from "@mui/icons-material/CloseOutlined";
import Link from "@mui/material/Link";
import WelcomeImage from "assets/images/welcome.png";
import { styled } from "@mui/material/styles";

const notification = [
  {
    text: "You have just made a sale. Your last collected data was sold for $7.40 on Oct 23",
    link: "Find out how to earn more money"
  },
  {
    text: "We have made some changes to how we operate as a business, check out what that means for you",
    link: "View details"
  },
  {
    text: "A data buyer in the automotive industry has bought your data recently",
    link: "View details"
  }
];

const Aside = styled(Box)(() => ({
  position: "fixed",
  right: "0",
  bottom: "0",
  top: "0",
  left: "0",
  marginLeft: "0 !important",
  zIndex: "1300",
  height: "100vh",
  width: "100%",
  background: "rgba(126, 126, 126, 0.8)"
}));

const Container = styled(Box)(() => ({
  position: "absolute",
  display: "flex",
  flexDirection: "column",
  right: "0",
  height: "100vh",
  width: "408px",
  padding: "24px",
  background: "#fff",
  boxShadow: "0px 2px 12px rgba(35, 39, 55, 0.15)",
  zIndex: "1400",
  transition: "0.2s",
  overflow: "auto"
}));

const Notifications = ({ notifications, closeNotifications }) => {
  const [list, setList] = useState(notification);

  return (
    notifications && (
      <Aside>
        <Container>
          <Box
            display="flex"
            alignItems="center"
            justifyContent="space-between"
          >
            <Box display="flex" alignItems="center">
              <NotificationsNoneIcon fontSize="large" />
              <Typography variant="h5" margin="0 10px">
                Notifications
              </Typography>
              <Chip
                label="Clear all"
                color="primary"
                variant="outlined"
                clickable
                onClick={() => setList(null)}
                disabled={!list}
              />
            </Box>

            <CloseOutlinedIcon
              onClick={() => {
                closeNotifications();
                setList(notification);
              }}
              cursor="pointer"
              color="rgba(35, 39, 55, 1)"
            />
          </Box>

          <Box sx={{ padding: "27px 0", flexGrow: 1 }}>
            {list?.map((item, i) => (
              <Box key={i} marginBottom="20px" borderBottom="1px solid #E2E2E3">
                <Typography variant="body1" color="#13182E">
                  {item.text}
                </Typography>
                <Link href="#" underline="none" lineHeight="46px">
                  {item.link}
                </Link>
              </Box>
            ))}

            {!list && (
              <Box
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                flexGrow={1}
              >
                <img
                  sx={{
                    width: 173,
                    marginBottom: "26px"
                  }}
                  alt="clear notificarions"
                  src={WelcomeImage}
                />

                <Typography variant="h6" color="#13182E">
                  You’re up to date
                </Typography>
              </Box>
            )}
          </Box>
        </Container>
      </Aside>
    )
  );
};

export default Notifications;
